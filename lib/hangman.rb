class Hangman
  attr_reader :guesser, :referee, :board
  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    @secret_word_length = @referee.pick_secret_word
    @board = Array.new(@secret_word_length, nil)
    @guesser.register_secret_length(@secret_word_length)
  end

  def take_turn
    guess = @guesser.guess
    matches = @referee.check_guess(guess)
    update_board(matches, guess)
    @guesser.handle_response(guess, matches)
  end

  def update_board(matched_indices, guess)
    matched_indices.each {|i| @board[i] = guess}
  end

end

class HumanPlayer
  def initialize(dictionary)
    @dicitionary = dictionary
  end

  def check_guess(guess)
    matches = []
    @secret_word.chars.each_with_index do |letter, i|
      matches << i if letter == guess
    end
    matches
  end

  def guess(board)
    puts "Guess a letter"
    gets.chomp
  end

  def register_secret_length(length)
    length
  end
end

class ComputerPlayer
  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(guess)
    matches = []
    @secret_word.chars.each_with_index do |letter, i|
      matches << i if letter == guess
    end
    matches
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.reject {|word| word.length != length}
  end

  def guess(board)
    letter_counter = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, i|
        letter_counter[word[i]] += 1 if letter == nil
      end
    end

    letters = letter_counter.sort_by {|letter, count| count}
    letters.last.first
  end

  def handle_response(guess, matched_indices)
    @candidate_words.each do |word|
      match = false

      word.chars.each_with_index do |letter, i|
        if matched_indices.include?(i) && letter == guess
          match = true
        elsif !matched_indices.include?(i) && letter == guess
          match = false
        end
      end

      @candidate_words.delete(word) if match == false
    end
  end


end
